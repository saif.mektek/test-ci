# [ v0.6.10 ] -  April 22, 2021.
- Added a feature

# [ v0.6.9 ] -  April 22, 2021.
- Remove unnecessary files from changelog.md

# [ v0.6.8 ] -  April 22, 2021.
- Remove Merge to master via pipeline commit from changelog

# [ v0.6.7 ] -  April 22, 2021.
- Rafactor changelog
- Refactor pipeline

# [ v0.6.6 ] -  April 21, 2021.
- Added CI_TOKEN_USER as a variable, later save them as a secret

# [ v0.6.0 ] -  April 21, 2021.
- Modified the feature
- Added a new button to the feature
- Added a feature
- Deleted unnecessary condition

# [ v0.5.27 ] -  April 21, 2021.
- Trigered manual deploy to release

# [ v0.5.26 ] -  April 21, 2021.
- Deleted a file
- Added a file

# [ v0.5.25 ] -  April 21, 2021.
- Added findutils package

# [ v0.5.23 ] -  April 21, 2021.
Added ci skip job
Final touch
