#!/bin/bash
# Run this with

uuid=$1
switch_branch=$2

TOKEN=$(curl https://accounts.acquia.com/api/auth/oauth/token -s --data-urlencode "client_id=$ACQUIA_API_KEY" --data-urlencode "client_secret=$ACQUIA_API_SECRET" --data-urlencode "grant_type=client_credentials" | jq -e -r '.access_token?')
curl -X POST \
      -H "Authorization:Bearer $TOKEN" \
      -H 'content-type:application/json' \
      --fail \
      --data '{"branch":"ci-'"$switch_branch"'"}' \
      "https://cloud.acquia.com/api/environments/$uuid/code/actions/switch"