#!/bin/bash
# Run this with bash push_artifact_for_release_to_acquia.sh VERSION and VERSION_DESCRIPTION

set -e

release_tag=$1
release_description=$2

remote_name="acquia"
repository_url="obermeyer@svn-23450.prod.hosting.acquia.com:obermeyer.git"
author_email=$(git log --format='%ae' HEAD^!)
author_name=$(git log --format='%an' HEAD^!)
build_prefix="ci"

# Ensure proper permissions on mounted key.
cp /root/.ssh/id_rsa_source /root/.ssh/id_rsa && chmod 400 /root/.ssh/id_rsa
git config --global user.email "$author_email"
git config --global user.name "$author_name"
git config --global push.default matching

# Add Acquia deploy repo as a remote and fetch
git remote add $remote_name $repository_url
git fetch $remote_name

# Remove files unnecessary for production build
rm .gitignore
rm -rf .composer
rm -rf .yarn-cache

# Remove .git directories from submodules, or else they won't be added/committed. Since we have several dependencies
# relying on a specific commit (i.e. dev branches), there is no other version available than the source version. The
# repositories must be removed, or `git add .` on the subsequent line will not add all necessary files.
find ./* -type d -name ".git" -exec rm -rf {} +

# Add production build files to deployment repository
git add .

# Manually add git-ignored directory
git add -f docroot/themes/custom/obermeyer/css
git commit -q -am "$release_description job id: $CI_JOB_ID"


# If build was triggered by a tag, use the tag name to create a similarly named
# tag on the destination.
destination_tag="$build_prefix-$release_tag"
git tag "$destination_tag" -f
git push $remote_name "HEAD:refs/tags/$destination_tag" -f
echo "... from tag $release_tag ..."

